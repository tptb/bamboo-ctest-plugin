package fr.cstb.bamboo.plugins.ctest;

import com.atlassian.bamboo.build.test.TestCollectionResult;
import com.atlassian.bamboo.build.test.TestCollectionResultBuilder;
import com.atlassian.bamboo.build.test.TestReportCollector;
import com.google.common.collect.Sets;
import org.apache.commons.io.IOUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Set;

public class CTestTestReportCollector implements TestReportCollector
{
    @NotNull
    @Override
    public TestCollectionResult collect(@NotNull File file) throws Exception
    {
        final CTestParser parser = new CTestParser();

        InputStream is = null;
        try
        {
            is = new FileInputStream(file);
            parser.parse(is);
        }
        finally
        {
            IOUtils.closeQuietly(is);
        }

        return new TestCollectionResultBuilder()
                .addFailedTestResults(parser.getFailedTests())
                .addSuccessfulTestResults(parser.getPassedTests()).build();
    }

    @NotNull
    @Override
    public Set<String> getSupportedFileExtensions()
    {
        return Sets.newHashSet("xml");
    }
}
