package fr.cstb.bamboo.plugins.ctest;

import com.atlassian.bamboo.build.test.TestCollationService;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import org.jetbrains.annotations.NotNull;

public class CTestTask implements TaskType
{
    private final TestCollationService testCollationService;

    public CTestTask(TestCollationService testCollationService)
    {
        this.testCollationService = testCollationService;
    }

    @NotNull
    @java.lang.Override
    public TaskResult execute(@NotNull final TaskContext taskContext) throws TaskException
    {
        final String pattern = taskContext.getConfigurationMap().get("testFilePathPattern");
        testCollationService.collateTestResults(taskContext, pattern, new CTestTestReportCollector());
        return TaskResultBuilder.create(taskContext).checkTestFailures().build();
    }
}