package fr.cstb.bamboo.plugins.ctest;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskTestResultsSupport;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

public class CTestTaskConfigurator extends AbstractTaskConfigurator implements TaskTestResultsSupport
{
    @Override
    public boolean taskProducesTestResults(@NotNull TaskDefinition taskDefinition)
    {
        return true;
    }

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        config.put("testFilePathPattern", params.getString("testFilePathPattern"));
        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        super.populateContextForCreate(context);

        context.put("testFilePathPattern", "**/Testing/*/*.xml");
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);

        context.put("testFilePathPattern", taskDefinition.getConfiguration().get("testFilePathPattern"));
    }

    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForView(context, taskDefinition);
        context.put("testFilePathPattern", taskDefinition.getConfiguration().get("testFilePathPattern"));
    }

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection)
    {
        super.validate(params, errorCollection);

        final String testFilePathPatternValue = params.getString("testFilePathPattern");
        if (StringUtils.isEmpty(testFilePathPatternValue))
        {
            errorCollection.addError("testFilePathPattern", getI18nBean().getText("fr.cstb.bamboo.plugins.ctest.testFilePathPattern.error"));
        }
    }
}
